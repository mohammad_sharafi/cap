import 'package:captain/logic/cubits/post_cubit.dart';
import 'package:captain/presentation/pages/post_page.dart';
import 'package:captain/presentation/screens/comment_screen.dart';
import 'package:cubit/cubit.dart' as cubit;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cubit/flutter_cubit.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';

class MyCubitObserver extends cubit.CubitObserver {
  @override
  void onTransition(cubit.Cubit cubit, cubit.Transition transition) {
    print(transition);
    super.onTransition(cubit, transition);
  }

}


void main()  async{
  WidgetsFlutterBinding.ensureInitialized();
  cubit.Cubit.observer = MyCubitObserver();
  HydratedBloc.storage = await HydratedStorage.build(
      storageDirectory: await getApplicationDocumentsDirectory(),
  );
  runApp(MultiCubitProvider(
    providers: [
      CubitProvider(create: (context) =>PostCubit()),


    ],
    child: new MaterialApp(
      title: 'Caption',
      initialRoute: '/',
      routes: {
      //  '//profile': (context) => Profile(),
        '/': (context) => CubitProvider(create: (context) => PostCubit(),child: PostPage(),),
        '/comments': (context) => CommentScreen(),
      },
      debugShowCheckedModeBanner: false,
    ),
  ));
}