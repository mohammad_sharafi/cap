import 'package:captain/data/models/post_model.dart';
import 'package:captain/logic/utility/custom-exeption.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

  Future<List<PostModel>> getPost(String pageNumber) async {
    List<PostModel> listModel = [];
    final response = await http.get(
        "https://607a9689bd56a60017ba2d1f.mockapi.io/api/v1/post/?page=$pageNumber&limit=25",

        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        }

    );
    var res = response.body;
    final resBody = jsonDecode(res);

    if(resBody!=null) {
      for (Map i in resBody)
        listModel.add(PostModel.fromJson(i));
      return listModel;
    }
    else
      throw CustomException('There is not any Item');
  }
  Future<List<PostModel>> SearchPost(String pagename) async {
    List<PostModel> listModel = [];
    final response = await http.get(
        "https://607a9689bd56a60017ba2d1f.mockapi.io/api/v1/post?title=${pagename}",

        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        }

    );
    var res = response.body;
    final resBody = jsonDecode(res);

    if(resBody!=null) {
      for (Map i in resBody)
        listModel.add(PostModel.fromJson(i));
      return listModel;
    }
    else
      throw CustomException('There is not any Item');
  }





