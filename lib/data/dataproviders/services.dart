import 'dart:async';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

class Services {
  Future<String> fromDataService(String url, FormData formData) async {
    var dio = Dio();
    try {
      final response = await dio.post(
        url,
        data: formData,
      );
      return response.toString();
    } on Exception
    catch (error) {
      throw Exception(error);
    }
  }


}