/// id : "1"
/// postId : "1"
/// createdAt : "2021-04-16T19:19:54.525Z"
/// user : "Ms. Corene Ziemann"
/// body : "I'll override the digital XSS capacitor, that should alarm the SAS feed!"

class Comments {
  String _id;
  String _postId;
  String _createdAt;
  String _user;
  String _body;

  String get id => _id;
  String get postId => _postId;
  String get createdAt => _createdAt;
  String get user => _user;
  String get body => _body;

  Comments({
    String id,
    String postId,
    String createdAt,
    String user,
    String body}){
    _id = id;
    _postId = postId;
    _createdAt = createdAt;
    _user = user;
    _body = body;
  }

  Comments.fromJson(dynamic json) {
    _id = json["id"];
    _postId = json["postId"];
    _createdAt = json["createdAt"];
    _user = json["user"];
    _body = json["body"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["postId"] = _postId;
    map["createdAt"] = _createdAt;
    map["user"] = _user;
    map["body"] = _body;
    return map;
  }

}