import 'comment_model.dart';

/// id : "1"
/// createdAt : "2021-04-17T03:58:01.464Z"
/// views : 97
/// published : true
/// title : "Small"
/// comments : [{"id":"1","postId":"1","createdAt":"2021-04-16T19:19:54.525Z","user":"Ms. Corene Ziemann","body":"I'll override the digital XSS capacitor, that should alarm the SAS feed!"},{"id":"38","postId":"1","createdAt":"2021-04-16T19:02:52.012Z","user":"Dillon Becker","body":"Use the wireless PNG monitor, then you can override the mobile circuit!"},{"id":"53","postId":"1","createdAt":"2021-04-16T22:15:40.384Z","user":"reza felegari","body":"We need to override the haptic CSS card!"},{"id":"54","postId":"1","createdAt":"2021-04-20T17:42:41.927Z","user":"Sister Leuschke","body":"If we quantify the matrix, we can get to the AGP port through the solid state ADP feed!"},{"id":"55","postId":"1","createdAt":"2021-04-20T16:01:37.794Z","user":"Mehran jafari","body":"some text!"},{"id":"56","postId":"1","createdAt":"2021-04-20T07:12:47.858Z","user":"mehran jafari2","body":"test"},{"id":"57","postId":"1","createdAt":"2021-04-19T23:27:28.419Z","user":"mehran jafari2","body":"test"},{"id":"58","postId":"1","createdAt":"2021-04-20T10:44:50.466Z","user":"mehran jafari2","body":"best app"},{"id":"66","postId":"1","createdAt":"2021-04-20T02:02:57.226Z","user":"shahrzad tt","body":"hi"},{"id":"68","postId":"1","createdAt":"2021-04-21T07:50:35.940Z","user":"Lue Conn","body":"Try to synthesize the IB bus, maybe it will copy the primary sensor!"},{"id":"69","postId":"1","createdAt":"2021-04-21T07:22:46.930Z","user":"Estell Morar","body":"Try to program the SCSI protocol, maybe it will hack the solid state bus!"},{"id":"70","postId":"1","createdAt":"2021-04-21T15:09:36.953Z","user":"gggggg","body":"gggggggg"}]

class PostModel {
  String _id;
  String _createdAt;
  int _views;
  bool _published;
  String _title;
  List<Comments> _comments;

  String get id => _id;
  String get createdAt => _createdAt;
  int get views => _views;
  bool get published => _published;
  String get title => _title;
  List<Comments> get comments => _comments;

  PostModel({
      String id,
      String createdAt, 
      int views, 
      bool published, 
      String title, 
      List<Comments> comments}){
    _id = id;
    _createdAt = createdAt;
    _views = views;
    _published = published;
    _title = title;
    _comments = comments;
}

  PostModel.fromJson(dynamic json) {
    _id = json["id"];
    _createdAt = json["createdAt"];
    _views = json["views"];
    _published = json["published"];
    _title = json["title"];
    if (json["comments"] != null) {
      _comments = [];
      json["comments"].forEach((v) {
        _comments.add(Comments.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["id"] = _id;
    map["createdAt"] = _createdAt;
    map["views"] = _views;
    map["published"] = _published;
    map["title"] = _title;
    if (_comments != null) {
      map["comments"] = _comments.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

