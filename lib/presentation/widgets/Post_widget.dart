import 'dart:convert';

import 'package:captain/constants/styles.dart';
import 'package:captain/data/models/post_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class PostWidget extends StatefulWidget {

  final PostModel post;
  final Function callBackAnswer;

  const PostWidget({
    Key key, this.post, this.callBackAnswer}) : super(key: key);

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> {

  TextEditingController controllerAnswer = TextEditingController();
  OverlayEntry overlayEntry;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String title,view_S;

    int viewInt=widget.post.views;
    int commentCount=widget.post.comments.length;
    String commentCount_S=commentCount.toString();
    title = widget.post.title;
    view_S =viewInt.toString();


    return Container(
      height:  85,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(title ?? widget.post.title, style: Styles.h1StyleBlack,),
          SizedBox(height:48,),
          Row(
            children: [
              Row(
                children: [
                  Icon(
                    Icons.comment,
                    color: Colors.pink[300],
                    size: 18.0,
                  ),
                  SizedBox(width: 5,),
                  Text(commentCount_S, style: Styles.h1Stylepink,),
                ],
              ),
              SizedBox(width:80,),
              Row(
              children: [
                Icon(
                  Icons.remove_red_eye_outlined,
                  color: Colors.pink[300],
                  size: 18.0,
                ),
                SizedBox(width: 5,),
                Text(view_S, style: Styles.h1Stylepink,),
              ],
            ),],
          )

        ],
      ),
    );
  }


}








