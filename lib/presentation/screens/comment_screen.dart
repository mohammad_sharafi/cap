import 'dart:convert';
import 'package:captain/data/dataproviders/service_url.dart';
import 'package:captain/data/dataproviders/services.dart';
import 'package:captain/data/models/comment_model.dart';
import 'package:captain/data/models/post_model.dart';
import 'package:dio/dio.dart';
import 'package:captain/presentation/widgets/dialog.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class CommentScreen extends StatefulWidget {
  @override
  _CommentScreenState createState() => _CommentScreenState();
}

class _CommentScreenState extends State<CommentScreen> {
  List<Comments> fetchedComments = [];
  final TextEditingController _commentController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  PostModel postModel;


  @override
  Widget build(BuildContext context) {
    postModel= ModalRoute.of(context).settings.arguments;
    fetchedComments=postModel.comments;

    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Comments",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: buildPage(),
    );
  }

  Widget buildPage() {
    return Column(
      children: [
        Expanded(
          child:
          buildComments(),
        ),
        Divider(),
        ListTile(
          title: TextFormField(
            controller: _commentController,
            decoration: InputDecoration(labelText: 'Write a comment...'),
            onFieldSubmitted: _addComment,
          ),
          trailing: OutlineButton(onPressed: (){_addComment(_commentController.text);}, borderSide: BorderSide.none, child: Text("Post"),),
        ),
        Divider(),
      ],
    );
  }


  Widget buildComments() {
      return Container(
        margin: const EdgeInsets.all(12),
        child: Expanded(
          child: ListView.separated(
            itemBuilder: (_,index)=>Text(this.fetchedComments[index].body),
            separatorBuilder: (_,__)=>Divider(), itemCount: fetchedComments.length,
          ),
        ),
      );
    }


  OverlayEntry overlayEntry;

  _showProgress() {
    overlayEntry = OverlayEntry(builder: (c) {
      return FunkyOverlay();
    });
    Overlay.of(context).insert(overlayEntry);
  }

  _removeProgress() {
    overlayEntry.remove();
  }
  _addComment(String comment) async {


    _showProgress();

    FormData formData = new FormData.fromMap({
      'user': 'Mohammad Sharafi',
      'body': '${_commentController.text}',
    });

    Services()
        .fromDataService('${ServiceURL.BaseUrl}/post/${postModel.id}/comment', formData)
        .then((value) {
      var res = (json.decode(value));
      print(res);
      if (res['status'] == 200 || res['status'] == 201) {
        _removeProgress();

        Toast.show("successfully done", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      }
      else {
        _removeProgress();
        Toast.show("Error in send data", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      }
    });

  }
}

