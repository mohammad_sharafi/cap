import 'package:captain/logic/cubits/post_cubit.dart';
import 'package:captain/presentation/widgets/Post_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:captain/data/models/post_model.dart' as P;
import 'package:flutter_cubit/flutter_cubit.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PostPage extends StatelessWidget {
  int value = 1;
  final myController = TextEditingController();

  @override
  void dispose() {
    myController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    context.cubit<PostCubit>().load(value);
    RefreshController _refreshController =
        RefreshController(initialRefresh: false);

    void _onRefresh() async {
      await Future.delayed(Duration(milliseconds: 1000));
      _refreshController.refreshCompleted();
    }

    void _onLoading() async {
      value = value++;
      context.cubit<PostCubit>().load(value);
      _refreshController.loadComplete();
    }

    return Scaffold(
        appBar: AppBar(
          actions: [
            IconButton(icon: Icon(Icons.search), onPressed: () {
              if(myController.text.isEmpty)
                context.cubit<PostCubit>().load(value);
                else {context.cubit<PostCubit>().Search(myController.text);}
            }),
            Expanded(
              child: SizedBox(
                height: 60,
                child: TextField(
                  controller: myController,
                  decoration: new InputDecoration(hintText: "Search"),
                ),
              ),
            ),
          ],
          centerTitle: true,
        ),
        body: Container(
            color: Colors.grey[300],
            height: double.infinity,
            child: CubitBuilder<PostCubit, PostState>(
              builder: (context, state) {
                if (state is PostInitial) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                } else if (state is PostFailed) {
                  return Center(child: Text(state.error));
                } else if (state is PostLoaded) {
                  return SmartRefresher(
                    enablePullUp: true,
                    footer: CustomFooter(
                      builder: (BuildContext context, LoadStatus mode) {
                        Widget body;
                        if (mode == LoadStatus.idle) {
                          body = Text("pull up load");
                        } else if (mode == LoadStatus.loading) {
                          body = CupertinoActivityIndicator();
                        } else if (mode == LoadStatus.failed) {
                          body = Text("Load Failed!Click retry!");
                        } else if (mode == LoadStatus.canLoading) {
                          body = Text("release to load more");
                        } else {
                          body = Text("No more Data");
                        }
                        return Container(
                          height: 55.0,
                          child: Center(child: body),
                        );
                      },
                    ),
                    controller: _refreshController,
                    onRefresh: _onRefresh,
                    onLoading: _onLoading,
                    child: ListView.builder(
                        itemCount:state.postModel.length,
                        itemBuilder: (context, index) {
                          return Card(
                            margin: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 20),
                            elevation: 1,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).pushNamed('/comments',
                                    arguments: state.postModel[index]);
                              },
                              child: Container(
                                margin: EdgeInsets.all(10),
                                // (state.exersice.content.answers[index].question.media != null && state.exersice.content.answers[index].question.media != '') ?200 :200,
                                width: double.infinity,
                                child: Center(
                                  child: PostWidget(
                                    post: P.PostModel(
                                        id: state.postModel[index].id,
                                        title: state.postModel[index].title,
                                        comments:
                                            state.postModel[index].comments,
                                        createdAt:
                                            state.postModel[index].createdAt,
                                        published:
                                            state.postModel[index].published,
                                        views: state.postModel[index].views),
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                  );
                } else {
                  return Center(child: Text('Error'));
                }
              },
            )));
  }
}

class NoItemsFound extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Icon(
          Icons.folder_open,
          size: 24,
          color: Colors.grey[900].withOpacity(0.7),
        ),
        const SizedBox(width: 10),
        Text(
          "No Items Found",
          style: TextStyle(
            fontSize: 16,
            color: Colors.grey[900].withOpacity(0.7),
          ),
        ),
      ],
    );
  }
}
