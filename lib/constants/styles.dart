import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Styles {

  static TextStyle h1StyleBlack = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 14.0,
      fontWeight: FontWeight.w500,
      color: Colors.black);
  static TextStyle h1Stylepink = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 13.0,
      fontWeight: FontWeight.w500,
      color: Colors.pink[300],);
  static TextStyle h4StyleBlack = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 16.0,
      fontWeight: FontWeight.w500,
      color: Colors.black54);

  static TextStyle h1StyleWhite = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 16.0,
      fontWeight: FontWeight.w500,
      color: Colors.white);
  static TextStyle h1StyleWhitesmall = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 13.0,
      fontWeight: FontWeight.w500,
      color: Colors.white);
  static TextStyle h1StyleWhiteBold = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 18.0,
      fontWeight: FontWeight.w500,
      color: Colors.white);
  static TextStyle h2StyleBlack = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 14.0,
      fontWeight: FontWeight.w500,
      color: Colors.black);
  static TextStyle Whitesmall = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 12.0,
      fontWeight: FontWeight.w500,
      color: Colors.white);

  static TextStyle h2StyleWhite = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 14.0,
//      fontWeight: FontWeight.w500,
      color: Colors.white);

  static TextStyle h2StyleWhiteBold = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 14.0,
      fontWeight: FontWeight.bold,
//      decoration: TextDecoration.b,
//      fontWeight: FontWeight.w500,
      color: Colors.white);

  static TextStyle h2StyleBlackBold = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 14.0,
      fontWeight: FontWeight.bold,
//      decoration: TextDecoration.b,
//      fontWeight: FontWeight.w500,
      color: Colors.black54);

  static TextStyle h4Style = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 13.0,
      wordSpacing: 2,
      fontWeight: FontWeight.w500,
      color: Colors.black54);

  static TextStyle h5StyleGray = TextStyle(
      fontFamily: 'IranSans',
      fontSize: 12.0,
      color: Colors.black);
}
