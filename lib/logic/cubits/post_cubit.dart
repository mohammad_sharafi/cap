import 'package:captain/data/models/post_model.dart';
import 'package:captain/data/repositories/repositories.dart';
import 'package:cubit/cubit.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'post_state.dart';

class PostCubit extends Cubit<PostState> {
  PostCubit() : super(PostInitial());


  void load(int number) async
  {
    try {
      emit(PostInitial());

      List<PostModel> posts = await getPost('${number}');



      if(posts != null)
        emit(PostLoaded(posts));
      else
      {
        emit(PostFailed('There is no post'));
      }
    }
    catch(error)
    {
      emit(PostFailed(error.toString()));
    }
  }

  void Search(String name) async
  {
    try {
      emit(PostInitial());

      List<PostModel> posts = await SearchPost(name);


      if(posts != null)
        emit(PostLoaded(posts));
      else
      {
        emit(PostFailed('There is no post'));
      }
    }
    catch(error)
    {
      emit(PostFailed(error.toString()));
    }
  }


}