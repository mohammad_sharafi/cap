part of 'post_cubit.dart';

abstract class PostState extends Equatable {
  const PostState();
}

class PostInitial extends PostState {
  @override
  List<Object> get props => [];
}

class PostLoaded extends PostState {
  List<PostModel> postModel;

  PostLoaded(this.postModel);

  @override
  List<Object> get props => [postModel];
}

class PostFailed extends PostState {
  String error;

  PostFailed(this.error);

  @override
  List<Object> get props => [error];
}
